
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class program {


    private static final String FILENAME = "D:\\oop-laboratory-2016\\Year 2\\Semester 2\\Fundamental Algorithms\\FA_lab1\\src\\main\\java\\in.txt";

    public void readFromFile(){
        int MAX_SIZE = 1000;
        int[] myArray = new int[MAX_SIZE];
        int i = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(FILENAME))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                myArray[i++] = Integer.parseInt(sCurrentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

    }

}