package game;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

import io.BoardPrinter;
import models.Entity;
import models.Hole;
public class BoardConfiguration {
	private static final int ROW=10;
	private static final int COLUMN=20;
	private final char[][] board= new char[ROW][COLUMN];
	
	public void updateBoard(ArrayList<Entity> entities){
		for(int i=0; i<ROW;i++){
			for (int j=0;j<COLUMN;j++){
				board[i][j]='.';
			}
		}
		
		for(Entity e: entities){
			int x= e.getPosition().x;
			int y=e.getPosition().y;
			if(board[x][y]!='B'){
				board[x][y]=e.getBoardRepresentation();	
			}
		}
		
	}
	
	
	public char[][] getBoard(){
		return this.board;
	}
	
	public void printBoard(BoardPrinter printer){
			printer.print(this);
		
	}
	
	public Point getRandomlyUnnoccupiedPosition() {
		Random r= new Random();
		int x=r.nextInt(ROW);
		int y=r.nextInt(COLUMN);
		
		while(board[x][y]!='.')
		{
			x=r.nextInt(ROW);
			y=r.nextInt(COLUMN);
	
		}
		return new Point(x,y);
	}
	
	public Point getRandomHoleLocation(ArrayList<Hole> holes){
		int holeIndex=new Random().nextInt(holes.size());
		return 	new Point(holes.get(holeIndex).getPosition());
		
	}
	

}
