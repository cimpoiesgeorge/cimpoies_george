package io;
import game.BoardConfiguration;
public interface BoardPrinter {
public void print(BoardConfiguration board);
}
