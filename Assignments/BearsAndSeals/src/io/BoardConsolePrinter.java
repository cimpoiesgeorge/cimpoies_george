package io;

import game.BoardConfiguration;

public class BoardConsolePrinter implements BoardPrinter{
	public void print(BoardConfiguration boardConfiguration){
		char[][] boardData=boardConfiguration.getBoard();
		int ROW=boardData.length;
		int COLUMN=boardData[0].length;
		for(int i=0;i<ROW;i++){
			for(int j=0;j<COLUMN;j++){
				System.out.printf("%c ",boardData[i][j]);
			}
			System.out.println();
		}
		
	}

}
