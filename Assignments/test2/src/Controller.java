public class Fox {
    int x;
    int y;
    public Fox(int x, int y)
    {
        this.x=x;
        this.y=y;
    }
    public int getX()
    {
        return this.x;
    }
    public int getY()
    {
        return this.y;
    }
    public void move()
    {
        Coord[] res = Helpful.getNearest('R', new Coord(this.x, this.y));
// if nearest rabbit is within 2 steps
        if(Math.abs(res[0].x-this.x)+Math.abs(res[0].y-this.y) <= 2)
        {
//verify if near a tunnel and near a hunter
            Coord[] tunnels = Helpful.getNearest('T', new Coord(this.x,
                    this.y));
            Coord rt = Helpful.getNearest('T', new Coord(this.x, this.y),
                    res[0]);
            if(Math.abs(tunnels[0].x-this.x)+Math.abs(tunnels[0].y-this.y) <=
                    2)
            {
//hunter need to be near the tunnel
                Coord[] hunters = Helpful.getNearest('H', new
                        Coord(tunnels[0].x, tunnels[0].y));
                if(Math.abs(tunnels[0].xhunters[0].x)+Math.abs(tunnels[0].y-hunters[0].y)
                        <= 2)
                {
//then the fox will go towards the tunnel...
                    this.x=tunnels[0].xfinish;
                    this.y=tunnels[0].yfinish;
                }
            }
            Coord r = Helpful.getNearest('R', new Coord(this.x, this.y),
                    res[0]);
            this.x=r.x;
            this.y=r.y;
            return;
        }
// no rabbit => go to tunnel
        res = Helpful.getNearest('T', new Coord(this.x, this.y));
        Coord r = Helpful.getNearest('T', new Coord(this.x, this.y), res[0]);
        this.x=r.x;
        this.y=r.y;
    }
}
public class Hunter {
    int x, y;
    public Hunter(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public void move()
    {
        Coord[] res = Helpful.getNearest('T', new Coord(this.x, this.y));
//the hunter is near a tunnel, so he will wait for foxes
        if(Math.abs(res[0].x-this.x)+Math.abs(res[0].y-this.y) <= 2)
        {
            return;
        }
        Coord r = Helpful.getNearest('T', new Coord(this.x, this.y), res[0]);
        this.x=r.x;
        this.y=r.y;
    }
}
public class Rabbit {
    int x, y;
    public int getX() {
        return x;
    }
    public Rabbit(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public void move()
    {
        Coord[] res = Helpful.getNearest('T', new Coord(this.x, this.y));
//the rabbit is already near a tunnel so he will just be eating carrots
        if(Math.abs(res[0].x-this.x)+Math.abs(res[0].y-this.y) <= 2)
        {
            return;
        }
        Coord r = Helpful.getNearest('T', new Coord(this.x, this.y), res[0]);
        this.x=r.x;
        this.y=r.y;
    }
}
public class Tunnel {
    int startx, starty, finishx, finishy;
    char id;
    public Tunnel(int startx, int starty, int finishx, int finishy, char id) {
        this.startx = startx;
        this.starty = starty;
        this.finishx = finishx;
        this.finishy = finishy;
        this.id = id;
    }
    public char getId()
    {
        return this.id;
    }
    public int getStartx() {
        return startx;
    }
    public int getStarty() {
        return starty;
    }
    public int getFinishx() {
        return finishx;
    }
    public int getFinishy() {
        return finishy;
    }
}
public class Main {
    Rabbit Rabbits[];
    int nrRabbits = 0;
    Fox Foxes[];
    int nrFoxes = 0;
    Tunnel Tunnels[];
    int nrTunnels = 0;
    Hunter Hunters[];
    int nrHunters = 0;
    static int n;
    static int m;
    public void writeCurrentState()
    {
//write matrix
        int i, j, rabbit, fox, tunnel, hunter;
        for(i=0; i<n; i++)
        {
            for(j=0; j<m; j++)
            {
                boolean Rabbit = false, Fox=false, printed=false;
                for(rabbit=0; rabbit<nrRabbits; rabbit++)
                {
                    if(Rabbits[rabbit].getX()==i &&
                            Rabbits[rabbit].getX()==j)
                    {
                        Rabbit=true;
                    }
                }
                for(fox=0; fox<nrFoxes; fox++)
                {
                    if(Foxes[fox].getX()==i && Foxes[fox].getX()==j)
                    {
                        Fox=true;
                    }
                }
                if(Rabbit && !Fox)
                {
                    System.out.print('R');
                    printed = true;
                }
                if(Fox && !Rabbit)
                {
                    System.out.print('F');
                    printed = true;
                }
                for(tunnel=0; tunnel<nrTunnels; tunnel++)
                {
                    if(Tunnels[tunnel].getStartx() == i &&
                            Tunnels[tunnel].getStarty()==j)
                    {
                        System.out.print(Tunnels[tunnel].getId());
                        printed=true;
                    }
                    if(Tunnels[tunnel].getFinishy() == i &&
                            Tunnels[tunnel].getFinishy()==j)
                    {
                        System.out.print(Tunnels[tunnel].getId());
                        printed=true;
                    }
                }
                for(hunter=0;hunter<nrHunters;hunter++)
                {
                    if(Hunters[hunter].getX()==i &&
                            Hunters[hunter].getY()==j)
                    {
                        System.out.print('H');
                        printed=true;
                    }
                }
                if(!printed)
                {
                    System.out.print('.');
                }
            }
            System.out.println();
        }
    }
    public static void oneStep()
    {
        int rabbit, fox, hunter;
        for(rabbit=0; rabbit<nrRabbits; rabbit++)
        {
            Rabbits[rabbit].move();
        }
        for(fox=0; fox<nrFoxes; fox++)
        {
            Foxes[fox].move();
        }
        for(hunter=0; hunter<nrHunters; hunter++)
        {
            Hunters[hunter].move();
        }
        updateCoordonatesInStructures();
        writeCurrentState();
    }
    public static void updateCoordonatesInStructures()
    {
// update every step
        nrFoxes = 0;
        nrRabbits = 0;
        nrHunters = 0;
        nrTunnels = 0;
        Coord[] items = Helpful.getItems();
        for(int i = 0; i<items.length; i++)
        {
            if(items.type=='F')
            {
                Foxes[nrFoxes] = new Fox(items.x, items.y);
                nrFoxes++;
            }
            if(items.type=='R')
            {
                Rabbits[nrRabbits] = new Rabbit(items.x, items.y);
                nrRabbits++;
            }
            if(items.type=='H')
            {
                Hunters[nrHunters] = new Hunter(items.x, items.y);
                nrHunters++;
            }
            if(items.type=='T')
            {
                Tunnels[nrTunnels] = new Tunnel(items.sx, items.sy,
                        items.fx, items.fy, items.id);
                nrTunnels++;
            }
        }
    }
    public static void main(String[] args) {
        n=15;
        m=86;
        updateCoordonatesInStructures();
        while(true)
        {
            oneStep();
            System.out.println("Press enter");
            System.in.read();
        }
    }
}