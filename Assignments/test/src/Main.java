class A {
    int f() { return 1; }
}
class B extends A {
    int f() { return 0; }
}
class Main {
    public static void main(String[] args) {
        A a = new B();
        System.out.println(a.f());
    }
}