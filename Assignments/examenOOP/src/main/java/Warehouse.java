/**
 * Created by George Cimpoies on 2/10/2017.
 */
public class Warehouse extends GameObject {

    public void setBurning(boolean burning) {
        isBurning = burning;
    }
    public boolean isBurning() {
        return isBurning;
    }

    public int getFireResistance() {
        return fireResistance;
    }

    public WarehouseType getWarehouseType() {
        return warehouseType;
    }

    private int fireResistance;

    private WarehouseType warehouseType;
    private boolean isBurning;
    public static final int FLAMMABLE_RESISTANCE = 0;
    public static final int COMBUSTIBLE_RESISTANCE = 3;

    public Warehouse(WarehouseType warehouseType){
        this.warehouseType = warehouseType;
        this.isBurning = false;
        if(warehouseType == WarehouseType.FLAMMABLE){
            this.fireResistance = FLAMMABLE_RESISTANCE;
        }
        else this.fireResistance = COMBUSTIBLE_RESISTANCE;
    }


}
