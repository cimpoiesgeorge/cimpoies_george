/**
 * Created by George Cimpoies on 2/8/2017.
 */

public abstract class GameObject {

    public boolean hasObjectNearby() {
        for (int i = 0; i < 40; i++) {
            for (int j = 0; j < 40; j++) {
                if (Controller.getMap()[i][j] == this) {
                    if (Controller.getMap()[i - 1][j - 1] == null
                            && Controller.getMap()[i - 1][j] == null
                            && Controller.getMap()[i - 1][j + 1] == null
                            && Controller.getMap()[i][j + 1] == null
                            && Controller.getMap()[i + 1][j + 1] == null
                            && Controller.getMap()[i + 1][j] == null
                            && Controller.getMap()[i + 1][j - 1] == null
                            && Controller.getMap()[i][j - 1] == null) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
