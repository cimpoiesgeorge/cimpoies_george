import java.util.Scanner;

/**
 * Created by George Cimpoies on 2/8/2017.
 */
public class Controller {

    public static GameObject[][] getMap() {
        return map;
    }

    private static GameObject[][] map = new GameObject[40][40];

    public static void setIsBurning(Warehouse warehouse) {
        warehouse.setBurning(true);
    }

    public static void putFireOut(Warehouse warehouse) {
        warehouse.setBurning(false);
    }


    public static void main(String[] args) {

        //Declaration and instantiation of a Game Map (40x40 matrix of objects)


        map[13][11] = new FireEngine();

        map[12][14] = new Warehouse(WarehouseType.FLAMMABLE);
        setIsBurning((Warehouse) map[12][14]);


        map[15][20] = new Warehouse(WarehouseType.COMBUSTIBLE);
        putFireOut((Warehouse) map[15][20]);



        Scanner scanner = new Scanner(System.in);
        String readString = scanner.nextLine();

        while (readString != null) {
            System.out.println("");

            if (readString.isEmpty()) {

                //Logic of the game

            }

            if (scanner.hasNextLine()) {
                readString = scanner.nextLine();
            } else {
                readString = null;
            }
        }
    }


}
