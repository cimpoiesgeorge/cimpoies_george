
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ALU is
Port(  sw : in STD_LOGIC_VECTOR (15 downto 0);
   MPGEnable: in std_logic ;
   Digits: out std_logic_vector(15 downto 0) ;
   CLK:in std_logic ;
   led : out STD_LOGIC_VECTOR (15 downto 0)
   );
   
end ALU;

architecture Behavioral of ALU is

signal A,B,C,SUM,DIF,SFTR,SFTL:STD_LOGIC_VECTOR(15 DOWNTO 0);
signal cnt :std_logic_vector(1 downto 0);

begin

A<=X"00" & sw(7 downto 0);
B<=X"00" & sw(15 downto 8);
C<= sw(15 downto 0);
SUM<= A+B;
DIF<= A-B;
SFTL<= C(13 downto 0) & "00";
SFTR<= "00" & C(15 downto 2);
--clk
  process(clk)
begin
    if(clk'event and clk='1') then 
            cnt <= cnt + 1;
    end if;
end process;

--mux
 process(SUM,DIF,SFTL,SFTR,cnt)
  begin
  case(cnt) is
         when "00" => DIGITS <= SUM;
         when "01" => DIGITS <= DIF;
         when "10" => DIGITS <= SFTL;
         when "11" => DIGITS <= SFTR;
         when others => DIGITS <= x"1111" ;
          end case;
          end process;
  led(15)<='1' when DIGITS=x"0000" else '0';
  led(14 downto 0) <= "000000000000000";        

end Behavioral;
