
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;




entity SSD is
    Port( 
        D0: in STD_LOGIC_VECTOR (3 downto 0);
        D1: in std_logic_vector(3 downto 0);
        D2: in std_logic_vector(3 downto 0);
        D3: in std_logic_vector(3 downto 0);
        catod: out std_logic_vector (6 downto 0);
        anod: out std_logic_vector(3 downto 0);
        clk: in std_logic );
 
end SSD;

architecture Behavioral of SSD is
    
    signal cnt: std_logic_vector(15 downto 0);
    signal mux1:std_logic_vector(3 downto 0);
    signal LED :std_logic_vector(6 downto 0);

begin
-- clock
  process(clk)
  begin
      if(clk'event and clk='1') then 
              cnt <= cnt + 1;
      end if;
  end process;
  
  --mux 1
  process(D0,D1,D2,D3,cnt)
  begin
  case(cnt(15 downto 14)) is
         when "00" => mux1 <= D0;
         when "01" => mux1 <= D1;
         when "10" => mux1 <= D2;
         when "11" => mux1 <= D3;
         when others => mux1 <= "1111" ;
          end case;
          end process;
          
    --mux 2
    process(cnt) is
    begin 
        case (cnt(15 downto 14)) is
            when "00" => anod <= "1110";
            when "01" => anod <= "1101";
            when "10" => anod <= "1011";
            when "11" => anod <= "0111";
            when others => anod <="0111";
        end case;
    end process;

--7 segment
    with mux1 Select
   catod<= "1111001" when "0001",   --1
         "0100100" when "0010",   --2
         "0110000" when "0011",   --3
         "0011001" when "0100",   --4
         "0010010" when "0101",   --5
         "0000010" when "0110",   --6
         "1111000" when "0111",   --7
         "0000000" when "1000",   --8
         "0010000" when "1001",   --9
         "0001000" when "1010",   --A
         "0000011" when "1011",   --b
         "1000110" when "1100",   --C
         "0100001" when "1101",   --d
         "0000110" when "1110",   --E
         "0001110" when "1111",   --F
         "1000000" when others;   --0
    
  
end Behavioral;
