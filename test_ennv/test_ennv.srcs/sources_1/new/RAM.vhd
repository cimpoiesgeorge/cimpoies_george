library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity RAM is
    Port ( clk : in STD_LOGIC;        
           we : in STD_LOGIC;
           addres : in STD_LOGIC_VECTOR (7 downto 0);
           DI : in STD_LOGIC_VECTOR (15 downto 0);
           DO : out STD_LOGIC_VECTOR (15 downto 0));
end RAM;

architecture Behavioral of RAM is

type ram_type is array(0 to 255) of std_logic_vector (15 downto 0);

signal ram :ram_type :=(
    x"0001",
    x"0002",
    x"0004",
    x"001A",
    x"0020",
    x"0120",
    x"0105",
    others =>"0001"
);
begin
process(clk)
    begin
        if rising_edge(clk) then
            if we = '1' then    
                ram(conv_integer(addres)) <= DI ;
                else
                DO <= ram(conv_integer(addres));
              end if ;
              end if;
    end process ;
    
end Behavioral;
