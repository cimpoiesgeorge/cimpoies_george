
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;



entity MPG is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR(4 downto 0);
           en : out STD_LOGIC_VECTOR(4 downto 0));
end MPG;

architecture Behavioral of MPG is

signal cnt:std_logic_vector(15 downto 0);
signal q0:std_logic_vector(4 downto 0) := "00000";
signal q1:std_logic_vector(4 downto 0) := "00000";
signal q2:std_logic_vector(4 downto 0) := "00000";

begin

    --counter
    process(clk)
    begin
    if(clk'event and clk='1') then 
            cnt <= cnt + 1;
    end if;
    end process;
    
    --register
    process(clk, btn)
    begin
        if (clk'event and clk='1') then
            if (cnt=x"FFFF") then
                q0 <= btn;
            end if;
        end if;
    end process;

    --D
    process(clk)
    begin
        if (clk'event and clk='1') then
            q1 <= q0;
            q2 <= q1;
        end if;
    end process;

    en <= q1 and (not q2);
    
end Behavioral;
