
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;




entity test_env is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0);
            reset:in std_logic
           );
          
end test_env;

architecture Behavioral of test_env is

    component MPG is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR(4 downto 0);
           en : out STD_LOGIC_VECTOR(4 downto 0));
    end component;

    component SSD is
    Port( 
        D0: in STD_LOGIC_VECTOR (3 downto 0);
        D1: in std_logic_vector(3 downto 0);
        D2: in std_logic_vector(3 downto 0);
        D3: in std_logic_vector(3 downto 0);
        catod: out std_logic_vector (6 downto 0);
        anod: out std_logic_vector(3 downto 0);
        clk: in std_logic );
    end component;
       
    component RAM is
        Port ( clk : in STD_LOGIC;        
               we : in STD_LOGIC;
               addres : in STD_LOGIC_VECTOR (7 downto 0);
               DI : in STD_LOGIC_VECTOR (15 downto 0);
               DO : out STD_LOGIC_VECTOR (15 downto 0));
   end component;
    
    component reg_file is
    Port (
       clk : in std_logic;
       ra1 : in std_logic_vector (2 downto 0);
       ra2 : in std_logic_vector (2 downto 0);
       wa : in std_logic_vector (2 downto 0);
       wd : in std_logic_vector (15 downto 0);
       regWr : in std_logic;
       rd1 : out std_logic_vector (15 downto 0);
       rd2 : out std_logic_vector (15 downto 0)
   );
   end component;
 
    signal ce:std_logic_vector(4 downto 0) := "00000";
    signal dcd:std_logic_vector(15 downto 0) ;
    signal decoded:std_logic_vector(7 downto 0);
    signal digits:std_logic_vector(15 downto 0);
    
    signal romOut:STD_LOGIC_VECTOR(15 downto 0); ---ROM
    signal rd1:std_logic_vector(15 downto 0); ---RF1
    signal rd2:std_logic_vector(15 downto 0); ---RF2
    signal wd:std_logic_vector(15 downto 0);
    signal wa:std_logic_vector(2 downto 0);
    signal ramOut :std_logic_vector(15 downto 0) ; ---RAM
    signal aluIn2 :std_logic_vector(15 downto 0) ; -- aluin B
    signal sum : std_logic_vector(15 downto 0); -- alu result
    signal display :std_logic_vector(15 downto 0) ;
   
     signal funct    : std_logic_vector(2 downto 0) ;
     signal opcode   : std_logic_vector(2 downto 0);
     signal sa       : std_logic;
    signal imm: std_logic_vector(15 downto 0);
    signal jump_addr: std_logic_vector(15 downto 0);
    signal pc_src:std_logic;
    signal jump:std_logic;
    signal RegWrite, Regwritei:std_logic;
    signal RegDst:std_logic;
    signal AluSrc:std_logic;
    signal MemWrite:std_logic;
    signal MemToReg:std_logic;
    signal ALUControl : std_logic_vector(2 downto 0);
    signal AluOp : std_logic_vector(2 downto 0);
    signal Branch:std_logic ;
    signal aluResAux:std_logic_vector(15 downto 0);
    signal zeroAux: std_logic;
     --------------------------------------------------
     
-------------------------------------------------------------------------
---ROM COMPONENT ---
type romArr is array (0 to 255) of std_logic_vector(15 downto 0);
signal rom:romArr :=(
    x"0520", --ADD  res: 00AB,001E=>00C9 --000_001_010_010_0_000
    x"1D20", --ADD  res: 00AB,001E=>00C9 --000_111_010_010_0_000
    x"0D30", --ADD  res: 00AB,001E=>00C9 --000_011_010_011_0_000

    x"8F01", --BEQ  res: 000A,000A=>     100_011_110_0000001
    x"E001", --JUMP res: 001E            111_0000000000001
    x"FFFF",
    others =>x"1011"   --  0001 0000 0001 0001
);
---------------------------------------------------
begin

   MyMPG: MPG port map(clk,btn,ce);
    
    -- program counter
    process(clk, ce, jump, pc_src) 
    begin
        if (clk'event and clk = '1') then
         if ce(0) = '1' then 
            if jump = '1' then 
                dcd <= "000" & romOut(12 downto 0); --jump address
            elsif pc_src = '1' then
                dcd <= dcd + 1 + imm;
            else 
                dcd <= dcd + 1;
            end if;
         elsif  ce(1) = '1' then 
            dcd <= (others => '0');
         end if;
        end if;
    end process;
------------------------------------------------------------------------------
    pc_src<= Branch and zeroAux;
    romOut <=rom(conv_integer(dcd(7 downto 0)));
    
    ---------------------------instruction decode------------------------------
    imm <= x"00" & '0' & romOut(6 downto 0);
    opcode <= romOut(15 downto 13);
    funct <= romOut(2 downto 0);
    sa <= romOut(3);
    
    wa <= romOut(9 downto 7) when RegDst = '0' else romOut(6 downto 4);
    
     MyRF : reg_file Port map(
       clk => clk ,
       ra1 => romOut(12 downto 10), -- rs
       ra2 => romOut(9 downto 7), -- rt
       wa => wa,
       wd => wd,
       rd1=> rd1,
       rd2=> rd2,
       regWr => Regwritei
       );
    
    Regwritei <= Regwrite and ce(0);
    
    aluIn2 <= rd2 when AluSrc = '0' else imm;
    -----------------------------ALU------------------
     process(ALUControl,rd1,aluIn2,sa,aluResAux)
     begin
     case(ALUControl) is
            when "000" => aluResAux<=rd1+aluIn2;   --ADD---
            when "001" => aluResAux <= rd1-aluIn2;   ---SUB---
            when "010" =>                               ---SLL--
                        case(sa) is
                         when '1' =>aluResAux<=rd1(14 downto 0) & "0" ;
                         when others => aluResAux <=rd1 ;
                         end case;
             when "011" =>                     ----SRL---
                              case(sa) is
                               when '1' =>aluResAux<= "0" & rd1(15 downto 1) ;
                                when others => aluResAux <=rd1 ;
                                 end case;    
            when "100" =>aluResAux <=rd1 and aluIn2;    ---AND---
            when "101" =>aluResAux <=rd1 or aluIn2;    ---OR----
            when "110" =>aluResAux <=rd1 xor aluIn2;   ----XOR---
            when "111" =>                              ---SET ON LESS THAN ---
                          if rd1<aluIn2 then
                                 aluResAux<=X"0001";
                          else aluResAux<=X"0000";
                                end if;
          when others => aluResAux <=X"0000"; 
          end case;
          
          case (aluResAux) is                    -----ZERO SIGNAL-----
                  when X"0000" => zeroAux<='1';
                  when others => zeroAux<='0';
              end case;
    end process ;
  
    
    MyRAM: RAM Port map(
         clk => clk,
         we=>MemWrite,
         addres=>sum(7 downto 0),
         DI=>rd2,
         DO=>ramOut
         );
     
    wd <= ramOut when MemToReg = '1' else aluResAux;
    
    --------------------------- control unit---------------------------------------
    
     
    led (15) <= RegDst;         
    led (14) <= AluSrc;                 
    led (13) <= Branch;
    led(12)  <=  jump;
    led(11)  <=  MemWrite;          
    led(10)  <=   MemToReg;           
    led(9)   <=  RegWrite ;   
    led(8)  <= zeroAux ;         
    led(7) <= '0';   
    led(6) <= '0';   
    led(5) <= '0';   
    led(4) <= '0';   
    led(3) <= '0';                                 
    led (2 downto 0) <= ALUControl;
    
    
    digits <= display;
    process(sw,romOut,rd1,rd2,imm,sum,ramOut,wd,dcd)
    begin
        case sw(2 downto 0) is
            when "000" =>display <= romOut;
            when "001" => display <=rd1;
            when "010" => display <=rd2;
            when "011" => display <=imm;
            when "100" => display <=aluResAux;
            when "101" => display <=ramOut;
            when "110" => display <=wd;
            when others => display <=dcd;  
        end case;
    end process;
    
    MySSD: SSD Port map ( 
        D0  => digits(3 downto 0), 
        D1  => digits(7 downto 4), 
        D2  => digits(11 downto 8), 
        D3  => digits(15 downto 12), 
        catod => cat, 
        anod    => an, 
        clk => clk
        );
 ------- Control unit
 process(opcode)
 begin
     case (opcode) is 
         when "000"=> --Instructiuni de tip R
             RegDst<='1';         
             AluSrc<='0';
             Branch<='0';
             jump<='0';
             ALUControl<=funct;
             MemWrite<='0';
             MemToReg<='0';
             RegWrite<='1';
             
         when "001"=> -----ADDI-----
             RegDst<='0';
             AluSrc<='1';
             Branch<='0';
             jump<='0';
             ALUControl<="000";
             MemWrite<='0';
             MemToReg<='0';
             RegWrite<='1';
             
         when "010"=> -----LW-----
             RegDst<='0';
             AluSrc<='1';
             Branch<='0';
             jump<='0';
             ALUControl<="000";
             MemWrite<='0';
             MemToReg<='1';
             RegWrite<='1';
             
         when "011"=> -----SW-----
             RegDst<='0';
             AluSrc<='1';
             Branch<='0';
             jump<='0';
             ALUControl<="000";
             MemWrite<='1';
             MemToReg<='0';
             RegWrite<='0';
             
         when "100"=> -----BEQ-----
             RegDst<='0';
             AluSrc<='0';
             Branch<='1';
             jump<='0';
             ALUControl<="001";
             MemWrite<='0';
             MemToReg<='0';
             RegWrite<='0';
             
         when "101"=> -----ANDI-----
             RegDst<='0';
             AluSrc<='1';
             Branch<='0';
             jump<='0';
             ALUControl<="100";
             MemWrite<='0';
             MemToReg<='0';
             RegWrite<='1';
             
         when "110"=> -----ORI-----
             RegDst<='0';
             AluSrc<='1';
             Branch<='0';
             jump<='0';
             ALUControl<="101";
             MemWrite<='0';
             MemToReg<='0';
             RegWrite<='1';
             
         when "111"=> -----JUMP-----
             RegDst<='0';
             AluSrc<='0';
             Branch<='0';
             jump<='1';
             ALUControl<="000";
             MemWrite<='0';
             MemToReg<='0';
             RegWrite<='0';
         
         when others =>    -----OTHERS-----
             RegDst<='0';
             AluSrc<='0';
             Branch<='0';
             jump<='0';
             ALUControl<="000";
             MemWrite<='0';
             MemToReg<='0';
             RegWrite<='0';
     end case;
 end process;    
 

                                   
end Behavioral;
